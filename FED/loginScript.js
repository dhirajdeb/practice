const loginPage = document.getElementById("login-page");
const username = document.getElementById("username");
const password = document.getElementById("password");
const submitButton = document.getElementById("submit-button");
const loginForm = document.getElementById("login-form");
const message = document.getElementById("message");
const remember = document.getElementById("remember");

// Mocking DB data
const userData = {
  username: "test",
  password: "test",
};

const redirectToDashboard = (e) => {
  e.preventDefault();
  const isUserValidated = !!localStorage.getItem("isUserValidated");

  if (isUserValidated) {
    console.log(isUserValidated);
    location.href = "./dashboard.html";
  }
};

loginPage.onload = redirectToDashboard;

submitButton.addEventListener("click", (e) => {
  e.preventDefault();
  const isValidUsername = userData.username === username.value;
  const isValidPassword = userData.password === password.value;
  const isValidUser = isValidUsername && isValidPassword;
  const isRememberSelected = !!remember.checked;

  if (isValidUser) {
    location.href = "./dashboard.html";

    if (isRememberSelected) {
      localStorage.setItem("isUserValidated", true);
    } else {
      localStorage.clear();
    }
  } else {
    message.innerHTML = "Invalid login credentials!";
  }
});
