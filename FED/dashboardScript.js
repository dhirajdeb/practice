const dashboardBody = document.getElementById("dashboard-body");
const dashboardTab = document.getElementById("dashboard-tab");
const catalogTab = document.getElementById("catalog-tab");
const dashboardDisplay = document.getElementById("dashboard-display");
const productCatalog = document.getElementById("product-catalog");
const logoutButton = document.getElementById("logout-button");
const hamburger = document.getElementById("hamburger");
const sidenav = document.getElementById("sidenav");
const dashboardBackground = document.getElementById("dashboard-background");

const highlightedTabStyle = (elem) => {
  elem.style.color = "black";
  elem.style.backgroundColor = "lightblue";
  elem.style.borderLeft = "3px solid blue";
};

const normalTabStyle = (elem) => {
  elem.style.color = "grey";
  elem.style.backgroundColor = "white";
  elem.style.border = "none";
};

const showDashboard = (e) => {
  e.preventDefault();
  highlightedTabStyle(dashboardTab);
  normalTabStyle(catalogTab);
  dashboardDisplay.style.display = "block";
  productCatalog.style.display = "none";
};

const showProducts = (e) => {
  e.preventDefault();
  highlightedTabStyle(catalogTab);
  normalTabStyle(dashboardTab);
  productCatalog.style.display = "block";
  dashboardDisplay.style.display = "none";
};

dashboardBody.onload = showDashboard;

dashboardTab.addEventListener("click", showDashboard);

catalogTab.addEventListener("click", showProducts);

logoutButton.addEventListener("click", (e) => {
  e.preventDefault();
  localStorage.clear();
  location.href = "./index.html";
});

hamburger.addEventListener("click", (e) => {
  e.preventDefault();
  sidenav.style.display = "block";
  sidenav.style.width = "200px";
});

dashboardBackground.addEventListener("click", (e) => {
  e.preventDefault();
  const isMobileDevice = window.matchMedia("(max-width: 600px)").matches;
  const clickedOnHamburger = e.target.id === "hamburger";
  const clickedOnHamburgerIcon = e.target.id === "hamburger-icon";
  const clickedOnSidebar = e.target.id === "sidenav";
  const clickedOnDashboardTab = e.target.id === "dashboard-tab";
  const clickedOnCatalogTab = e.target.id === "catalog-tab";
  const outSideClickConditions = [
    clickedOnHamburger,
    clickedOnHamburgerIcon,
    clickedOnSidebar,
    clickedOnDashboardTab,
    clickedOnCatalogTab,
  ];
  const outSideClick = !outSideClickConditions.some((elem) => !!elem);

  if (outSideClick && isMobileDevice) {
    sidenav.style.display = "none";
    sidenav.style.width = "0px";
  }
});

window.addEventListener("resize", (e) => {
  const notMobileDevice = !window.matchMedia("(max-width: 600px)").matches;

  if (notMobileDevice) {
    sidenav.style.display = "block";
    sidenav.style.width = "200px";
  } else {
    sidenav.style.display = "none";
    sidenav.style.width = "0px";
  }
});
